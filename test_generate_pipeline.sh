#!/bin/bash

set -o errexit

__DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

[[ ! -f "${__DIR}/credentials.yml" ]]
  cp credentials-sample.yml credentials.yml

if [[ -f "${__DIR}/pipeline.yml" ]]; then
  echo -n "You already have a pipeline.yml. Are you sure you want to overwrite? [Y/N]: "
  read overwrite
  shopt -s nocasematch
  if [[ $overwrite != "y" ]]; then
    exit 0
  fi
fi

trap 'rm generated_pipeline.yml' EXIT
echo "---" > "${__DIR}/generated_pipeline.yml"
cat snippets/resources.yml >> "${__DIR}/generated_pipeline.yml"
echo "jobs:" >> "${__DIR}/generated_pipeline.yml"

echo "Hello, "$USER".  This script will create a Concourse pipeline for your microservice."
echo -n "Enter your project name and press [ENTER]: "
read projectName
cat snippets/generate-version.yml >> "${__DIR}/generated_pipeline.yml"
cat snippets/build-and-upload.yml >> "${__DIR}/generated_pipeline.yml"
echo -n "Does your pipeline need to test for api compatibility? [Y/N]: "
read compatibility
shopt -s nocasematch
if [[ $compatibility = "y" ]]; then
  cat snippets/build-api-compatibility-check.yml >> "${__DIR}/generated_pipeline.yml"
fi
cat snippets/publish-quality-metrics.yml >> "${__DIR}/generated_pipeline.yml"
cat snippets/test-deploy.yml >> "${__DIR}/generated_pipeline.yml"
cat snippets/test-smoke.yml >> "${__DIR}/generated_pipeline.yml"

# echo -n "Does your microservice call other microservices that need to be stubbed? [Y/N]: "
passed="test-smoke"
echo -n "Does your pipeline need to test for rollback? [Y/N]: "
read rollback
shopt -s nocasematch
if [[ $rollback = "y" ]]; then
  cat snippets/test-rollback-deploy.yml >> "${__DIR}/generated_pipeline.yml"
  cat snippets/test-rollback-smoke.yml >> "${__DIR}/generated_pipeline.yml"
  passed="test-rollback-smoke"
fi
ruby -rerb -e "@passed='${passed}'; puts ERB.new(File.read('snippets/stage-deploy.yml.erb')).result(binding)" >> "${__DIR}/generated_pipeline.yml"
cat snippets/stage-e2e.yml >> "${__DIR}/generated_pipeline.yml"
cat snippets/prod-deploy.yml >> "${__DIR}/generated_pipeline.yml"
cat snippets/prod-complete.yml >> "${__DIR}/generated_pipeline.yml"
cat snippets/params.yml >> "${__DIR}/generated_pipeline.yml"

cat "${__DIR}/generated_pipeline.yml"
echo -n "Do you want to save this pipeline? [Y/N]: "
read save
shopt -s nocasematch
if [[ $save = "y" ]]; then
  cp ${__DIR}/generated_pipeline.yml ${__DIR}/pipeline.yml
fi
