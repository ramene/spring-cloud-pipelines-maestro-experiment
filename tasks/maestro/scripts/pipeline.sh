#!/bin/bash


function createPipeline {
  local maestro_repo_dir="${1}"
  local appName="${2}"
  local github_private_key="${3}"
  local params_file="${maestro_repo_dir}/apps/${appName}/params.yml"
  local compatibility="${4:-y}"
  local rollback="${5:-y}"

  echo "maestro_repo_dir is [${maestro_repo_dir}]"
  echo "appName is [${appName}]"
  echo "params_file is [${params_file}]"
  echo "compatibility is [${compatibility}]"
  echo "rollback is [${rollback}]"

  if [[ ! -f "${params_file}" ]]; then
    echo "Parameters not found for app [${appName}] in folder [${maestro_repo_dir}/apps/${appName}]"
    exit 1
  fi

  cd "${maestro_repo_dir}"

  echo "Generating pipeline"
  trap 'rm ${maestro_repo_dir}/generated_pipeline.yml' EXIT
  echo "---" > "generated_pipeline.yml"
  cat snippets/resources.yml >> "generated_pipeline.yml"
  echo "jobs:" >> "generated_pipeline.yml"

  cat snippets/generate-version.yml >> "generated_pipeline.yml"
  cat snippets/build-and-upload.yml >> "generated_pipeline.yml"
  shopt -s nocasematch
  if [[ $compatibility = "y" ]]; then
    cat snippets/build-api-compatibility-check.yml >> "generated_pipeline.yml"
  fi
  cat snippets/publish-quality-metrics.yml >> "generated_pipeline.yml"
  cat snippets/test-deploy.yml >> "generated_pipeline.yml"
  cat snippets/test-smoke.yml >> "generated_pipeline.yml"

  passed="test-smoke"
  shopt -s nocasematch
  if [[ $rollback = "y" ]]; then
    cat snippets/test-rollback-deploy.yml >> "generated_pipeline.yml"
    cat snippets/test-rollback-smoke.yml >> "generated_pipeline.yml"
    passed="test-rollback-smoke"
  fi
  ruby -rerb -e "@passed='${passed}'; puts ERB.new(File.read('snippets/stage-deploy.yml.erb')).result(binding)" >> "generated_pipeline.yml"
  cat snippets/stage-e2e.yml >> "generated_pipeline.yml"
  cat snippets/prod-deploy.yml >> "generated_pipeline.yml"
  cat snippets/prod-complete.yml >> "generated_pipeline.yml"
  cat snippets/params.yml >> "generated_pipeline.yml"
  cd ..
  echo y | ./fly -t "${appName}" sp -p "${appName}" -c "${maestro_repo_dir}/generated_pipeline.yml" -l "${params_file}" -v "github-private-key=${github_private_key}"
}

function runPreHookScript {
  maestro_repo_dir="${1}"
  appName="${2}"
  params_file="${maestro_repo_dir}/apps/${appName}/params.yml"

  echo "maestro_repo_dir is [${maestro_repo_dir}]"
  echo "appName is [${appName}]"
  echo "params_file is [${params_file}]"

  set +e
  prehook_script=$(grep "pre-hook-script:" $params_file | grep "^[^#;]" | cut -d " " -f 2)
  prehook_params=$(grep "pre-hook-params:" $params_file | grep "^[^#;]" | cut -d " " -f 2)
  set -e
  if [[ -f "${maestro_repo_dir}/apps/${appName}/${prehook_script}" ]]; then
    echo "Executing [${maestro_repo_dir}/apps/${appName}/${prehook_script}] with args [${prehook_params}]"
    . "${maestro_repo_dir}/apps/${appName}/${prehook_script}" "${prehook_params}"
  fi
}

# This function contains the list of scripts to process pipeline YAML patches for ALL apps.
# Typically, the execution of these scripts should be controlled by a flag in
# the credentials.yml file.
function processPipelinePatchesForAllApps() {
  echo "Global pipeline patches processing."
  # exit 0
}

# This function contains the list of scripts to process pipeline YAML patches for each application.
# Typically, the execution of these scripts should be controlled by a flag in
# the app configuration file (./apps/${app}/params.yml), which path is provided the "$app" variable
function processPipelinePatchesPerApp() {
  appName="${1}"
  paasType="${2}"

  echo "Application pipelines patches: preparing template files for each task."
  # exit 0
}

# Generic utility functions for yaml patching
function removeTaskFromJob() {
  fileToPatch="${1}"
  parentJobName="${2}"
  taskName="${3}"

  cat > remove_task_from_job.yml <<EOF
---
- op: remove
  path: /jobs/name=$parentJobName/task=$taskName
EOF
  cp $fileToPatch ./removeTaskFromJob-tmp.yml
  cat ./removeTaskFromJob-tmp.yml | yaml_patch_linux -o ./remove_task_from_job.yml > $fileToPatch
}

# applies the maestro resource addition patch to a pipeline
function applyMaestroResourcePatch() {
  pipelineFile="${1}"

  cp $pipelineFile ./applyMaestroResourceTmp.yml
  cat ./applyMaestroResourceTmp.yml | yaml_patch_linux -o ./operations/opsfiles/pcf-pipelines-maestro-resource.yml > $pipelineFile
}
