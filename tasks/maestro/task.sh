#!/bin/bash

export ROOT_FOLDER=$( pwd )
export REPO_RESOURCE=spring-cloud-pipelines-maestro
export TOOLS_RESOURCE=spring-cloud-pipelines

echo "Root folder is [${ROOT_FOLDER}]"
echo "Repo resource folder is [${REPO_RESOURCE}]"
echo "Tools resource folder is [${TOOLS_RESOURCE}]"

for script in ${ROOT_FOLDER}/${REPO_RESOURCE}/tasks/maestro/scripts/*.sh; do
  source $script
done

processDebugEnablementConfig "credentials.yml"  # determine if script debug needs to be enabled

previous_concourse_url="" # initialize current concourse url variable

cc_url=$MAIN_CONCOURSE_URL
cc_main_user=$MAIN_CONCOURSE_USERNAME
cc_main_pass=$MAIN_CONCOURSE_PASSWORD
skip_ssl_verification=$MAIN_CONCOURSE_SKIP_SSL

# prepare Concourse FLY cli in the task container (see ./tasks/maestro/scripts/tools.sh)
prepareTools "${cc_url}"

loginConcourseTeam "${cc_url}" "${cc_main_user}" "${cc_main_pass}" "main" "${skip_ssl_verification}"

# Process pipelines YAML patches that will apply to all apps
# See function definition in ./operations/operations.sh
processPipelinePatchesForAllApps

for app in ${ROOT_FOLDER}/${REPO_RESOURCE}/apps/*; do

    # parse app name
    appName=$(basename "$app")

    echo "Processing pipelines for application [${appName}]"

    concourseFlySync "${cc_url}" "${previous_concourse_url}" "main"
    previous_concourse_url=$cc_url     # save current concourse url

    # create concourse team for the app if not existing yet (see ./tasks/maestro/scripts/concourse.sh)
    createConcourseTeam "${appName}" "${cc_main_user}" "${cc_main_pass}" "main"

    # Login into the corresponding Concourse team for the foundation (see ./tasks/maestro/scripts/concourse.sh)
    loginConcourseTeam "${cc_url}" "${cc_main_user}" "${cc_main_pass}" "${appName}" "${skip_ssl_verification}"

    # Process pipelines YAML patches for each app according to its configuration (see ./operations/operations.sh)
    # processPipelinePatchesPerApp "${appName}" "cf"

    # Create the concourse pipeline
    createPipeline "${ROOT_FOLDER}/${REPO_RESOURCE}" "${appName}" "${GITHUB_PRIVATE_KEY}" "y" "y"

    runPreHookScript "${ROOT_FOLDER}/${REPO_RESOURCE}" "${appName}"
done
