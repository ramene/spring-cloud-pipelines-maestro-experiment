#!/bin/bash
set -e

lowerCaseProjectType=$( echo "${PROJECT_TYPE}" | tr '[:upper:]' '[:lower:]' )
__DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

[[ -f "${__DIR}/pipeline-${lowerCaseProjectType}-custom.sh" ]] && source "${__DIR}/pipeline-${lowerCaseProjectType}-custom.sh" || \
    echo "No pipeline-${lowerCaseProjectType}-custom.sh found"