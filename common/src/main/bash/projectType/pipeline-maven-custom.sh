#!/bin/bash

function build() {
    echo "Additional Build Options [${BUILD_OPTIONS}]"

    version=$( retrieveVersion )
    echo "Setting version to [${version}]"

    ./mvnw versions:set -DnewVersion=${version} ${BUILD_OPTIONS}
    if [[ "${CI}" == "CONCOURSE" ]]; then
        ./mvnw clean verify deploy -Ddistribution.management.release.id=${M2_SETTINGS_REPO_ID} -Ddistribution.management.release.url=${REPO_WITH_BINARIES} ${BUILD_OPTIONS} || ( $( printTestResults ) && return 1)
    else
        ./mvnw clean verify deploy -Ddistribution.management.release.id=${M2_SETTINGS_REPO_ID} -Ddistribution.management.release.url=${REPO_WITH_BINARIES} ${BUILD_OPTIONS}
    fi
}

function retrieveVersion() {
    local version=${PIPELINE_VERSION}
    if [ "${USE_PIPELINE_VERSION}" = false ]; then
      local currentVersion=$( retrieveCurrentVersion )
      version=${currentVersion:-${PIPELINE_VERSION}}
    fi
    echo "${version}"
}

function retrieveCurrentVersion() {
    local result=$( ruby -r rexml/document -e 'puts REXML::Document.new(File.new(ARGV.shift)).elements["/project/version"].text' pom.xml || ./mvnw ${BUILD_OPTIONS} org.apache.maven.plugins:maven-help-plugin:2.2:evaluate -Dexpression=project.version |grep -Ev '(^\[|Download\w+:)' )
    result=$( echo "${result}" | tail -1 )
    echo "${result}"
}
