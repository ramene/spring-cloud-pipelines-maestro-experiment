# spring-cloud-pipelines-maestro-experiment

Please see [149975591](https://www.pivotaltracker.com/story/show/149975591) for more info.

```
 fly -t docker sp -p spring-cloud-pipelines-maestro -c pipeline-maestro.yml -l credentials.yml
```